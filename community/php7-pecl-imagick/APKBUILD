# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Valery Kartel <valery.kartel@gmail.com>
pkgname=php7-pecl-imagick
_extname=imagick
pkgver=3.5.0_rc2
_pkgver=${pkgver/_rc/RC}
pkgrel=0
pkgdesc="PHP 7 extension provides a wrapper to the ImageMagick library - PECL"
url="https://pecl.php.net/package/imagick"
arch="all !x86" # https://gitlab.alpinelinux.org/alpine/aports/-/issues/12537
license="PHP-3.01"
depends="php7-common imagemagick"
checkdepends="ghostscript-fonts"
makedepends="php7-dev imagemagick-dev"
subpackages="$pkgname-dev"
source="php-pecl-$_extname-$_pkgver.tgz::https://pecl.php.net/get/$_extname-$_pkgver.tgz"
builddir="$srcdir/$_extname-$_pkgver"
provides="php7-imagick=$pkgver-r$pkgrel" # for backward compatibility
replaces="php7-imagick" # for backward compatibility

# secfixes:
#   3.4.4-r0:
#     - CVE-2019-11037

build() {
	phpize7
	./configure --prefix=/usr --with-php-config=php-config7
	make
}

check() {
	make NO_INTERACTION=1 REPORT_EXIT_STATUS=1 test TESTS=--show-diff
}

package() {
	make INSTALL_ROOT="$pkgdir" install

	local _confdir="$pkgdir"/etc/php7/conf.d
	mkdir -p $_confdir
	echo "extension=$_extname" > $_confdir/$_extname.ini
}

sha512sums="
819b1bf134c5d131fd34cebef02f3fa1432982e2c9fab866fd818d0af0839eecfd7fee65252a0537db36f60db10f8fea94950e7ade5c8a442a6f0b2bab9f6fbb  php-pecl-imagick-3.5.0RC2.tgz
"
