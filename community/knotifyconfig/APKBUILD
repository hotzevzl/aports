# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=knotifyconfig
pkgver=5.82.0
pkgrel=0
pkgdesc="Configuration system for KNotify"
# armhf blocked by extra-cmake-modules
# mips64, s390x and riscv64 blocked by polkit
arch="all !armhf !mips64 !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-only"
depends_dev="
	kcompletion-dev
	kconfig-dev
	ki18n-dev
	kio-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/knotifyconfig-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
d214d8019179c079a22f4076bb985f1528292c467cce36a18e2457a4051264f915f48a35ea2b508b900fded020bf20c8d3e784149e9be7993a80a027a70871e3  knotifyconfig-5.82.0.tar.xz
"
